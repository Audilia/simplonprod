<?php

add_action( 'admin_menu', 'admin_link' );

function admin_link() {
      add_menu_page(
        'My First Page', // titre de la page
        'My First Plugin', // le texte affiché sur le menu
        'manage_options',
        '../wp-content/plugins/monplugin/inclus/admin-page.php' // The 'slug' - le fichier a afficher au clique du lien
    );
}

