<?php
/*
Plugin Name: Mon premier plugin
Author: Elodie Jumel
*/

require_once plugin_dir_path(__FILE__) . 'inclus/monplugin-functions.php';
 
// Hook the 'wp_footer' action hook, add the function named 'text' to it
add_action("wp_footer", "text");
 
// Define 'text'
function text() {
  echo "<p style='color: black; background-color: #d5ffa0; display: flex; justify-content: center'>Voici un texte qui s'affiche grâce au plugin!</p>";
}

function shortcode_categorys($param) {
  extract(
      shortcode_atts(
          array(),
        $param
      )
  );

global $post;

$category = wp_get_post_terms($post->ID, 'category');
if (!empty($category)) {
  $category_name = $category[0]->name;
  $category_slug = $category[0]->slug;
}

$posts_in_category = get_posts(
  array(
      'orderby' => 'date',
      'order' => 'ASC',
      'tax_query' => array(
          array(
              'taxonomy' => 'category',
              'field' => 'slug',
              'terms' => $category_slug
          )
      )
  )
);

$category_data = array();

foreach ($posts_in_category as $k=>$post_in_category) {
      if ($post->ID == $post_in_category->ID) { $current_post_index = $k+1; }

      $category_data[] = array(
              'id' => $post_in_category->ID,
              'title' => $post_in_category->post_title
      );
}

// Afficher la liste des articles de notre série
foreach ($category_data as $article) {
      if ($article['id'] == $post->ID) {
              echo '<li><strong>' . $article['title'] . '</strong></li>';
      } else {
              echo '<li><a href="' . get_permalink($article['id']) . '" title="' . esc_attr($article['title']) . '">' . $article['title'] . '</a></li>';
      }
}
echo '</ol>';
echo '</section>';

$post_category_data = ob_get_contents();
ob_end_clean();

return $post_category_data;
}

add_shortcode('categorys', 'shortcode_categorys');




