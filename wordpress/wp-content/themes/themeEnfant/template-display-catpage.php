<?php
/**
* Template Name: Page des catégories
**/

get_header();

$my_posts = new WP_Query(array('post_type' => 'post', 'posts_per_page' => '20', 'category' => $title_page));

$title_page = get_the_title();
?>

<?php
	if($my_posts->have_posts()) :
?>
	<?php while($my_posts->have_posts()) : $my_posts->the_post(); ?>
		
	<div class="article-par-article">
        <?php echo '<li><a href="' . get_permalink(the_title()) . '</a></li>'; ?>
	</div>
	<?php endwhile; ?>
<?php else: ?>
	<p>Aucune article a été trouvé.</p>
<?php endif; ?>

<?php get_footer(); ?>
